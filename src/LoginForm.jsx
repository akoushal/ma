import React, { useState } from "react";
const LoginForm = () => {
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const handleEmail = (event) => {
    setEmail(event.target.value);
  };
  const handlePassword = (event) => {

    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    var allUsers = JSON.parse(localStorage.getItem("users"));
    if(allUsers){
      var result = allUsers.filter((value)=>{
        return value.email===email && value.password===password
      })
    }
    if(result&&result.length){
      localStorage.setItem('loginUser',JSON.stringify(result[0]))
      window.location.reload()
    }else{
      alert("User credential not valid !")
    }
  };
    return (
    <>
      <div className="container">
        <form
          className="border rounded bg-info shadow p-5 m-5"
          onSubmit={handleSubmit}
        >
          <div className="text-center h3 mb-4">Login Form</div>
          <div className="my-3 py-2">
            <label>Email</label>
            <input
              type="text"
              className="w-100 p-2 h6 mt-2 border-0 border-bottom border-dark bg-warning"
              placeholder="Enter Name"
              value={email}
              pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
              required
              onChange={handleEmail}
            />
          </div>
          <div className=" my-3 py-2">
            <label htmlFor="user-password">Password</label>
            <input
              id="user-password"
              className="w-100 p-2 h6 mt-2 border-0 border-bottom border-dark bg-warning"
              type="password"
              placeholder="Enter Password"
              value={password}
              required
              title="Password must contain atleast 8 digits, 1 uppercase letter, 1 lowercase and 1 numeric digit in it"
              onChange={handlePassword}
            />
            <span></span>
          </div>
          <div className="text-center mt-5">
            <button type="submit" className="btn btn-success fw-bold px-5">
              Login
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
export default LoginForm;