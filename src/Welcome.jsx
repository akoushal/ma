import React from "react";

const Welcome = () => {
  const user = JSON.parse(localStorage.getItem("loginUser"));

  const handleLogout = () => {
    localStorage.removeItem("loginUser")
    window.location.reload()
  };
  return (
    <>
      <div className="text-center m-5">
        <h1>Welcome {user.name}</h1>
        <button onClick={handleLogout}>Logout</button>
      </div>
    </>
  );
};
export default Welcome;
