import React, { useState } from "react";
const RegistrationForm = () => {
  const [myName, setMyName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [password, setPassword] = useState("");
  const handleName = (event) => {
    setMyName(event.target.value);
  };
  const handleMobile = (event) => {
    setMobile(event.target.value);
  };
  const handleEmail = (event) => {
    setEmail(event.target.value);
  };
  const handlePassword = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const userData = {
      name: myName,
      email: email,
      password: password,
      mobile: mobile,
    };
    var allUsers = JSON.parse(localStorage.getItem("users"));
    if (allUsers) {
      let user = allUsers.filter((user) => user.email === email);
      //console.log(user);
      if (user&&user.length) {
        alert(
          "This mail is already registered ,Please change this mail address."
        );
      }else{
        allUsers.push(userData);
        localStorage.setItem("users", JSON.stringify(allUsers));
      }
    } else {
      localStorage.setItem("users", JSON.stringify([userData]));
    }
    setEmail('')
    setMyName('')
    setMobile('')
    setPassword('')
  };
  return (
    <>
      <div className="container">
        <form
          className="border rounded bg-warning shadow p-5 m-5"
          onSubmit={handleSubmit}
        >
          <div className="text-center h3 mb-4">Registration Form</div>
          <div className="my-3 py-2">
            <label htmlFor="user-name">Name</label>
            <input
              id="user-name"
              type="text"
              className="w-100 p-2 h6 mt-2 border-0 border-bottom border-dark bg-warning"
              placeholder="Enter Name"
              value={myName}
              required
              onChange={handleName}
            />
          </div>
          <div className=" my-3 py-2">
            <label htmlFor="user-mobile">Mobile</label>
            <input
              id="user-mobile"
              className="w-100 p-2 h6 mt-2 border-0 border-bottom border-dark bg-warning"
              type="text"
              placeholder="Enter Mobile Number"
              value={mobile}
              pattern="[6-9]{1}[0-9]{9}"
              required
              title="Phone number with 6-9 and remaing 9 digit with 0-9"
              onChange={handleMobile}
            />
          </div>
          <div className="my-3 py-2">
            <label htmlFor="user-email">Email</label>
            <input
              id="user-email"
              type="email"
              className="w-100 p-2 h6 mt-2 border-0 border-bottom border-dark bg-warning"
              placeholder="Enter  Email Address"
              value={email}
              pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
              required
              // pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
              onChange={handleEmail}
            />
          </div>
          <div className=" my-3 py-2">
            <label htmlFor="user-passsword">Password</label>
            <input
              id="user-passsword"
              className="w-100 p-2 h6 mt-2 border-0 border-bottom border-dark bg-warning"
              type="password"
              placeholder="Enter Password"
              value={password}
              // pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
              required
              title="Password must contain atleast 8 digits, 1 uppercase letter, 1 lowercase and 1 numeric digit in it"
              onChange={handlePassword}
            />
          </div>

          <div className="text-center mt-5">
            <button type="submit" className="btn btn-success fw-bold px-5">
              Signup
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
export default RegistrationForm;
