import "./App.css";
import HelloFunc from "./HelloFunc";
import HelloClass from "./HelloClass";
import UserForm from "./UserForm";
import HelloWithMap from "./HelloWithMap";
import RegistrationForm from "./RegistrationForm";
import LoginForm from "./LoginForm";
import Home from "./Home";
import { BrowserRouter } from "react-router-dom";
import AuthRoute from "./Routes/AuthRoute";
import UnAuthRoute from "./Routes/UnAuthRoute";
import { useEffect, useState } from "react";

function App() {
  const friend = ["Mohini", "Abhishek", "Chanchal", "Aman", "Anshul"];
  const friendsDetails = [
    { name: "Mohini", salary: "20k" },
    { name: "Abhishek", salary: "25k" },
    { name: "Chanchal", salary: "22k" },
    { name: "Aman", salary: "20k" },
    { name: "Anshul", salary: "23k" },
  ];
  const [isLogin, setIsLogin] = useState(false)
  useEffect(() => {
    const user = JSON.parse(localStorage.getItem('loginUser'))
    if(user){
        setIsLogin(true)
    }
  }, [])
  return (
    <>

      {/* <HelloFunc counter = {0} /> */}
      {/* <HelloClass/> */}
      {/* <UserForm/> */}
      {/* {friend.map((value, index) => (
        <HelloWithMap name={value} key={index} />
      ))}
      {friendsDetails.map((value, index) => (
        <HelloWithMap name={value.name} salary={value.salary} key={index} />
      ))}
      {friendsDetails.map(({ name, salary }, index) => (
        <HelloWithMap name={name} salary={salary} key={index} />
      ))} */}
    <BrowserRouter>
    {isLogin?    <AuthRoute/>:    <UnAuthRoute/>}
    </BrowserRouter>
    </>
  );
}

export default App;
